import * as firebase from 'firebase';
import firestore from 'firebase/firestore';

const settings = {timestampsInSnapshots: true};

 // Your web app's Firebase configuration
 var firebaseConfig = {
    apiKey: "AIzaSyCnGf-BD5bFIWJeM5z4xUUBTO1U43xxQ7c",
    authDomain: "djamware-67ff5.firebaseapp.com",
    databaseURL: "https://djamware-67ff5.firebaseio.com",
    projectId: "djamware-67ff5",
    storageBucket: "djamware-67ff5.appspot.com",
    messagingSenderId: "1058379581246",
    appId: "1:1058379581246:web:f9e1466801d0ec16"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

firebase.firestore().settings(settings);

export default firebase;
